const fs = require('fs-extra');

async function init(path){
  await fs.ensureDir(path);
  await fs.copy(`content/static`,`${path}`,{dereference:true});
}
init('generated/toPublish');
