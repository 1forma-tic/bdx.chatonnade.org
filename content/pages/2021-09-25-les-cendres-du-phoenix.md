title: 🎯 Les cendres du phoenix 🎯
-------------------

![frontpage](img/bannieres/phoenix-cendre-nue-femme-auteur-ludovic-florent.jpg)
Photographe : [Ludovic Florent](http://www.edacc.fr/2014/11/poussiere-d-etoiles-des-danseuses-posent-pour-le-photographe-ludovic-florent.html)

# 🎯 Les cendres du phoenix 🎯
<!-- 🎯🧠 -->

L'équipe de Chatonnade Bordeaux n'a pas survécu à l'été.

A survécu chez moi l'envie d'expérimenter des formes d'interactions, de libertés, de vivre ensemble, de résiliences inaténiable sans créer un environnement favorable à ces explorations.

Les ingrédients de cet environnement favorable, et comment les assembler, c'est ça que je vous propose de co-créer.

Bref, choisir ensemble ce que nous voulons créer.


## 🦄 Qui est bienvenue ? 🦄

Toutes personnes souhaitant façonner un environnement, une communauté, une culture favorisant l'épanouissement de chacun⋅e.

Ceci dit, souhaitant former une communauté ou peuvent se sentir bien une grande diversité de personnes,
je souhaite favoriser cette diversité en gardant minoritaire dans les groupes et la gouvernance la catégorie sociologique la plus sur-représentée dans les médias et les organes décisionnels de notre société
(homme cis blanc hétéro décomplexé par l'age et le porte-feuille) et inclure prioritairement les catégories qui s'en éloignent.
En gros trouver un compromis entre : 
- retourner l'échelle de privilège occidentale (patriarcale, classiste, capitaliste, paternaliste, collonialiste, raciste, agiste, validiste...) pour inclure les plus oppressé⋅e⋅s
- inclure les personnes intéressées sans discriminations ni pré-jugés qu'ils soit issue du système dominant ou en réaction à celui-ci, donc inclure aussi les personnes les plus privilégiées.
Et que ce compromis ne reproduise pas ce contre quoi il lutte (donc qu'il ne génère pas de contre-classes, de contre-opressions... qui dans le champs de la communauté deviendrais des oppressions et des barrières de classes).

Bref, bienvenue en mixité inclusive (en conscience des travers de certaines formes de dogme de l'inclusivité finalement excluante).

Tout le monde est bienvenu indépendamment de son genre, orientation sexuelle et romantique, atypies, morphologie, ethnicité, nationalité, âge, religion, statut professionnel, expérience sexuelle, kinks, cycle corporel (menstruation), déclencheurs et traumatismes.

Si faire garder vos enfants devait vous empécher de venir, venez avec, et prévenez-nous qu'un espace leur soit dédié pour qu'ils puissent passer un bon moment dans un cadre adapté à leur besoins.


## 👥 Nombre de participant⋅e⋅s 👥

- idéalement autour de 7.
- audela de 12, j'aurais beosin d'aide pour faciliter en plusieurs groupes
- en dessous de 4, l'objectif évolura probablement sur comment être plus nombreux.

Pour que le groupe soit harmonieux, la mécanique de [pré-inscription](#-pré-inscription-) puis validation sera utilisé (et sa gouvernance pourra être discuté).
Elle permet notament d'optimiser la diversité des participant⋅e⋅s, et de former un groupe capable de fonctionner les un⋅e⋅s avec les autres.

## ♦️ Le cadre ♦️

Pour avancer efficacement ensemble, voici mes intensions de facilitation :
- **prendre soin des prise de parôle**
  - ne pas se couper la parôle
  - privilégier les personnes qui ont peu pris la parole
  - inviter à lever le doigt pour demander la parole
  - ou à demander une incise (question/clarification en une phrase max à formuler comme en réponse, histoire de pouvoir reprendre le fils)
- **prendre soin de la répartition de la parole**
  - privilégier les personnes qui ont peu pris la parole (bis)
  - proposer de mesurer les temps de parole par personne ou par genre, ou autre critère selon les envie des présent⋅e⋅s.
  - limiter les prises de parole récurente par les mêmes personnes en prennant le temps de l'offrir aux autres avant de la redonner au même.
- **prendre soin de l'état émotionnel des participant⋅e⋅s**
  - donner l'occasion à chacun⋅e de déposer son état émotionnel inital et final (par un tour de météo ou un check émotionnel en sous groupe, de l'écoute empatique en duo...)
  - observer, questionner, proposer de l'écoute au besoin
  - laisser des temps d'accueil émotionnel quand souhaité
  - prioriser ce qui est vivant plutôt que le respect d'un programme établi
  - questionner la place que le groupe as pour accueillir ce qui est au dépend du programme (et faire des sous-groupe si besoin pour répondre à chaque priorité)
- **prendre soin de la direction commune qui nous réunie** (désir, intension, raison d'être)
  - vérifier en amont (questionnaire) et en arrivant (cercle d'ouverture) que les intensions des présent⋅e⋅s sont suffisement proche pour coexister harmonieusement / pour être compatibles entre elles / pour former une intension commune.
  - questionner si ce qui à lieu est toujours à service de l'intension commune annoncée lorsque j'ai un doute.
  - informer des débordement horaire par rapport au déroulé proposé pour que prioriser de continuer ou réajuster puisse se faire en conscience.
- **favoriser la résilience de la communauté**
  - limiter les angles mort et stimuler l'empathie en créant des temps pour exprimer nos deuil, tristesses, frustration, honte et culpabilité...
  - aller chercher les raisons des exclusions silencieuses quand elles sont détectée par quelqu'un⋅e.
  - accompagner la sortie de l'impuissance face aux situations moins qu'idéale en aidant à faire émerger des stratégies satisfaisante.
  - offrir un espace de célébration et de gratitude pour les apprentissages joyeux et les réussite individuelles comme collectives.
- **encourager chacun⋅e à être soi**
  - valoriser l'authenticité d'être, la vulnérabilité émotionnelle et le non-jugement qui permet d'oser l'être (authentique et vulnérable).
  - valoriser le respect de ses limites et l'écoute de soi. Suivre ses pieds, accueillir avec gratitude les non pour les rendre plus fluide et léger...


## 🕗 Comment ? Horaires et déroulé 🕗

<!--

## 🕗 Horaires et programme 🕗

- Accueil de 15h00 à 15h30 (pour la cohésion du groupe, nous n’accepterons pas les arrivées ultérieures)
- 15h30 : Cercle d’ouverture
- ≈ 16h : Ateliers d’exploration de la culture Chatonnade (tout est facultatif)
  - ≈ 16h00 par l’intellectuel, le mental
  - ≈ 16h30 par l’émotionnel
  - ≈ 17h30 par le corps, sans contact avec les autres
  - ≈ 18h00 par le corps, en lien avec les autres
- ≈ 18h30 : Expérimentation libre, dans le respect du cadre et de la sécurité qu’il apporte
- ≈ 19h15 : Cercle de clôture
- 20h au plus tard, moment convivial autour d’un buffet
- ≈ 21h prolongation informelle éventuelle + rangement
- 23h au plus tard, extinction des feux, tout le monde dehors.


## 💸 Participation financière 💸

- Nous souhaitons être transparentes quant aux postes de dépenses liés à l’évènement.

  ⇒ [Comptabilité publiquement accessible](https://bdx.chatonnade.org/2021-07-25-welcat-compta.html)

- Nous souhaitons permettre à toute personne de participer sans condition de moyens.

  ⇒ Prix libre avec recommandations et [Free Money sur place](free-money.html)

- Nous souhaitons pouvoir organiser de futurs évènements à l’image de celui-ci.

  ⇒ Couvrir les frais + pérenniser l’implication des orga. Voir : [compta.](https://bdx.chatonnade.org/2021-07-25-welcat-compta.html)

Bref, nous serions soulagé⋅e⋅s et reconnaissant⋅e⋅s de recevoir 1000 € pour couvrir les frais de nourriture, du lieu, et le temps consacré par l’équipe pour l’évènement.

Pour atteindre cet objectif dans une société où monétairement, la pauvreté est bien plus commune que la richesse, notre proposition, c’est de vous orienter sans vous contraindre.

*__Info. :__ pour faire partie des 1% les plus riches au monde, gagner 30 000 € / an suffit.
([Source](https://s.42l.fr/1-percent-world-richest))*

- **Prix libre** possible pour n’exclure personne.

  ⇒ Si c’est destabilisant de choisir sans aide, les propositions suivantes sont pour vous.
- Participation aux frais (PaF solidaire) : 5 €

  ⇒ Les fins de mois sont difficiles ? C'est ok ! (1€ aussi, vu que d’autre mettent plus)
- Participation équilibre : 30 €

  ⇒ Dans un monde égalitaire : le prix recommandé à toustes.
- Participation soutien : 55 €

  ⇒ Un peu de rab ? Vous nous facilitez la vie !
- Participation justice sociale : 99 €

  ⇒ Pas besoin de compter vos sous ? Permettez à d'autres de venir.


Vous trouverez en bas de document le lien de pré-inscription.

PS : Pour soutenir le travail de fond
(coordination, documentation, conception d’outils…),
si un premier évènement vous a donnée l'envie de revenir,
nous vous encourageons à [souscrire à l'adhésion semestrielle libre](https://belong.li/home/community/ZSYj3I9nZdsjJAWE6hj7)
(avec montants suggérés) de Chatonnade.


## ⚕️ Mesures sanitaires ⚕️

- Si vous pouvez vous faire dépister la veille de l’évènement (ou être vacciné), c’est top !
  Note : *La Pharmacie du centre commercial Meriadeck (et probablement d'autres) propose des dépistages gratuit avec carte vitale, et résultats en 25 minutes.*
  Il y aura aussi sur place quelques auto-test Covid pour dépanner si besoin.
- Si vous présentez des symptômes du Covid avant l’évènement, prenez soin de votre santé et de celle des autres, ne venez-pas, nous vous rembourserons.
- Si vous présentez des symptômes du Covid dans les 7 jours suivant l’évènement, prévenez-nous, que nous transmettions l’information.
- Vos coordonnées vous seront demandées à l’inscription, elles pourront également servir à vous informer si vous devenez cas contact suite à l’évènement.
- Nous aérerons le lieu autant que possible durant toute la durée de l’évènement.
- De par les activités proposées durant l’évènement, il ne nous semble pas pertinent de réclamer le port du masque, ni d’en dissuader l’usage.


## 🏡 Lieu 🏡

Proche Bordeaux, accessible en Tram. Adresse exacte communiquée après validation de votre inscription.

Plusieurs espaces seront accessibles pendant l’événement avec des priorités différentes (calme, échange…).

## 📋 Questionnaire 📝
-->
<!-- 📋📝 -->

<!--

## 📅 Pré-inscription 📅


- **En ligne**

  Plus rapide et plus pratique pour nous, tout en préservant de notre mieux votre vie privée.

  [Remplissez le questionnaire de pré-inscription](https://framaforms.org/pre-inscription-welcat-25072021-1625588482)

- **Alternative papier**

  Si vous préférez le papier aux inscription en ligne, voici le [questionnaire de pré-inscription en pdf](pdf/Pre-inscription-WelCat-25_07_2021.pdf)
  vous pouvez l'imprimer, le remplir et nous l'envoyer à "Labôrizon, 26 Ter rue André Lapelleterie, 33130 Bègles".

  Si besoin, nous pouvons l'imprimer pour vous et que vous veniez le chercher sur rendez-vous à la même adresse (Tram C Arrêt la Belle Rose).
  Vous pouvez demander un rdv par sms au 0 770 772 770.

Quel que soit le mode de pré-inscription, voici les étapes suivantes :

3. Patientez, l'orga en charge des inscriptions (**lead participantes** dans notre jargon) va choisir qui valider pour cet évènement et les prévenir.
   Nous sélectionnons les participant⋅e⋅s pour avoir une diversité de profils propice à l’épanouissement de tous⋅tes les présent⋅e⋅s.
4. Vous êtes validé⋅e ? C'est le moment de retourner voir les recommandations sanitaires pour l'évènement.

Vous n'êtes pas retenu⋅e pour cet évènement ? Le prochain sera peut-être le bon !

PS : si vous n'avez pas été sélectionné⋅e de façon répétée, n'hésitez pas à en parler aux orga.


## 🌻 D’autres questions ? 🌻

Email contact : chatonnade.bordeaux@protonmail.com
A très vite ! ♥
-->
