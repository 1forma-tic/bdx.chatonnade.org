title: Compta Wel'Cat
-------------------
# Comptabilité Wel'Cat

Au jour du : 07/07/2021

Grace à l'évènement précédent, dont vous pouvez [consulter la comptabilité](2021-06-19-welcat-compta.html),
nous commençons avec :

- Fond de roulement : 60€
  <br>(trésorerie issue d'évènements passés pour avancer les frais de l'actuel)

- Free Money : 3€
  <br>(reste d'évènements passés, solidarité transversale entre participant⋅e⋅s)

- Safer Covid : 10 auto-test + Gel Hydro (acheté pour 46,20€)

Nous avions en revanche sous estimé le budget nourriture (7,33€/personne au lieu de 5€/personne).

En réutilisant le format Wel'Cat conçu pour l'évènement précédent,
nous devrions avoir besoin de moins de temps de préparation.

Ceci dit nous continuons à souhaiter soutenir les personnes qui ont consacré de leur temps à rendre cet évènement possible.
Qu’elles puissent renouveler leur implication au-delà de ce que leur temps libre seul leur permettrait.
Et ainsi qu’elles puissent activement contribuer au succès de nos futurs évènements.

Nous restons donc avec le même objectif financier pour cet évènement (1000€). Nous espérons que cela permettra une répartition d'avantage
pour soutenir nos orgas précaires que seulement pour couvrir les frais directs de l'évènement.

Ceci dit, vu que les frais de cet évènement seront probablement plus bas,
la PaF¹ solidaire suggérée baisse de 10 à 5€.

¹ PaF : Participation aux Frais

Dans tout les cas, vous pouvez vérifier que ce qui est fait de vos contributions financières vous conviens.
En effet, nous publions ici les montants collecté et la répartition ([comme pour l'évènement précédent](2021-06-19-welcat-compta.html))
et j'aurais plaisir à l'expliquer d'avantage si certaines affectations vous semble inadaptés.

Vous pouvez également venir participer au cercle de répartition
pour intervenir au moment où les décisions de répartition sont prises.



## Entrées :

### État actuel :
- Total (avec 0 participant⋅e⋅s) : 0 €
#### Détails :

## Sorties / Répartition :
### Légende :
- [ ] À rembourser/transmettre
- [x] Terminé

### État actuel :

- [x] 44,51€ de nourriture
  <br> (conservée pour majorité jusqu'au prochain évènement. Déduit des 60€ de fond de roulement)

<!--
Nous souhaitons aussi soutenir les personnes qui ont consacré de leur temps à rendre cet évènement possible.
Qu’elles puissent renouveler leur implication au-delà de ce que leur temps libre seul leur permettrait.
Et ainsi qu’elles puissent activement contribuer au succès de nos futurs évènements.

Rien qu’avec les temps de réunion et d’animation, nous dépassons les 50 heures pour cet évènement.
- [ ] 600 € à répartir entre les 4 orga pour le temps à concevoir, faire connaître, préparer, sécuriser et animer l’évènement.
-->

<!--

Au smic net horaire (8,11 de l’heure), cela donnerait déjà 405€.
Avec les cotisations et reversements, on dépasse les 600 €.
Facturé en entreprise, on dépasseraient les 2000€.
Bref, si nous ne voulons pas que l’implication des organisateurices se fasse au dépend
de leur finance, 600€ de soutien nous semble

Pour que leur implication de pèse pas sur leur vie mais l’allège.
Pour que cela soutienne leur vie plutôt que se faire à leur dépend.
Ni à proprement parlé bénévole, ni salarié, nous souhaitons soutenir les…
Pour que l’implication des organisateurices se fasse au service de leur vie
et non au détriment de cette dernière, nous visons de pouvoir les
soutenir financièrement dans un ordre de grandeur similaire au smic
pour les (> 50) heures consacrées à l’évènement.

-->
