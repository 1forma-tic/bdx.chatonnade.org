title: 💚 Wel'Cat 💚
-------------------

![frontpage Wel'Cat](img/bannieres/wel-cat.png)


# 💚 Wel’Cat 💚

🐱 Découvre lae Chatoune en toi ! 🐱

Bienvenue dans l’univers Chatonnade ! Les événements Chatonnade sont des espaces dans lesquels la liberté d’être, de connexion à soi et aux autres sont à l’honneur. Ils ont pour cœur des valeurs de consentement, d’inclusivité et de prendre soin.

💚 Événement de découverte sans sexualité

L’idée de ce premier événement est de faire découvrir la culture Chatonnade, à travers trois dimensions : la réflexion, l’émotionnel et le corporel.

- 💭 L’axe communication / réflexion se compose d’abord d’une présentation de Chatonnade, des valeurs et du cadre proposé. Puis, pour mettre en jeu ces valeurs, des exercices d’intelligence collective seront proposés pour explorer ensemble ces différents sujets.
- 💮 La partie émotionnelle vise à accéder à plus d’authenticité dans le rapport à soi et aux autres, grâce à des ateliers autour des émotions.
- 🐈 Enfin, la dimension corporelle met en avant le mouvement, le rapport à son corps dans l’espace, puis invite à explorer du contact avec les autres participant⋅e⋅s.

Les ateliers et jeux seront guidés par notre équipe d’organisatrices. Le consentement sera notre fil rouge tout au long de ce parcours, pour favoriser la sécurité et le lien.


## ♦️ Le cadre ♦️

Nous proposons un cadre pour favoriser la sécurité de chacun⋅e et prendre soin de la place de tous.tes :

- Invitation à l’authenticité, seul⋅e ou en groupe, et au respect de ses limites.
- La liberté de choisir est au centre : tu peux te retirer d’un exercice / atelier à tout moment, tu peux quitter l’événement si tu ne t’y sens pas bien, tu peux rester en retrait sur une partie et être plus actif⋅ve à d’autres moments.
- La liberté d’être seul⋅e prime sur la liberté d’être en lien
- Des angels seront présent⋅e⋅s tout au long de l’événement pour apporter de l’écoute, du soutien émotionnel aux participant⋅e⋅s qui en auraient besoin
- Cet événement est prévu sans sexualité ni nudité
- Respect de la confidentialité des participant⋅e⋅s
- Pas d’enregistrement (audio, photo, vidéo) pendant l’événement
- Invitation à ne pas consommer de substances et à ne pas être dans un état de conscience modifié pendant l’événement


## 🦄 Qui peut participer ? 🦄

Tout le monde est bienvenu indépendamment de son genre, orientation sexuelle et romantique, atypies, morphologie, ethnicité, nationalité, âge, religion, statut professionnel, expérience sexuelle, kinks, cycle corporel (menstruation), déclencheurs et traumatismes.

- Cet événement est destiné prioritairement aux personnes venant pour la première fois à l'un de nos événements, même si toute personne même habituée est bienvenue.
- Avoir au moins 18 ans


## 👥 Nombre de participant⋅e⋅s 👥
- 25 maximum pour vous fournir la qualité de cadre et d’animation à laquelle nous tenons.
- 10 minimum pour avoir des chances de rentrer dans nos frais et qu’il y ait plus de participant⋅e⋅s que d’orga.

Pour favoriser l’acceptation de la diversité en créant des espace plus sûrs pour tou⋅te⋅s, nous nous efforçons de constituer un groupe harmonieux, équilibré autant que possible sur le genre, l’orientation, l’expérience des milieux sexpo, avec une vigilance à l’inclusivité des personnes appartenant à des groupes discriminés.


## 🕗 Horaires et programme 🕗

- Accueil de 15h00 à 15h30 (pour la cohésion du groupe, nous n’accepterons pas les arrivées ultérieures)
- 15h30 : Cercle d’ouverture
- ≈ 16h : Ateliers d’exploration de la culture Chatonnade (tout est facultatif)
  - ≈ 16h00 par l’intellectuel, le mental
  - ≈ 16h30 par l’émotionnel
  - ≈ 17h30 par le corps, sans contact avec les autres
  - ≈ 18h00 par le corps, en lien avec les autres
- ≈ 18h30 : Expérimentation libre, dans le respect du cadre et de la sécurité qu’il apporte
- ≈ 19h15 : Cercle de clôture
- 20h au plus tard, moment convivial autour d’un buffet
- ≈ 21h prolongation informelle éventuelle + rangement
- 23h au plus tard, extinction des feux, tout le monde dehors.


## 💸 Participation financière 💸

- Nous souhaitons être transparentes quant aux postes de dépenses liés à l’évènement.

  ⇒ [Comptabilité publiquement accessible](https://bdx.chatonnade.org/2021-06-19-welcat-compta.html)

- Nous souhaitons permettre à toute personne de participer sans condition de moyens.

  ⇒ Prix libre avec recommandations

- Nous souhaitons pouvoir organiser de futurs évènements à l’image de celui-ci.

  ⇒ Couvrir les frais + pérenniser l’implication des orga. Voir : [compta.](https://bdx.chatonnade.org/2021-06-19-welcat-compta.html)

Bref, nous serions soulagé⋅e⋅s et reconnaissant⋅e⋅s de recevoir 1000 € pour couvrir les frais de nourriture, du lieu, et le temps consacré par l’équipe pour l’évènement.

Pour atteindre cet objectif dans une société où monétairement, la pauvreté est bien plus commune que la richesse, notre proposition, c’est de vous orienter sans vous contraindre.

*__Info. :__ pour faire partie des 1% les plus riches au monde, gagner 30 000 € / an suffit.
([Source](https://s.42l.fr/1-percent-world-richest))*

- **Prix libre** possible pour n’exclure personne.

  ⇒ Si c’est destabilisant de choisir sans aide, les propositions suivantes sont pour vous.
- Participation aux frais (PaF solidaire) : 10 €

  ⇒ Les fins de mois sont difficiles ? C'est ok ! (1€ aussi, vu que d’autre mettent plus)
- Participation équilibre : 30 €

  ⇒ Dans un monde égalitaire : le prix recommandé à toustes.
- Participation soutien : 55 €

  ⇒ Un peu de rab ? Vous nous facilitez la vie !
- Participation justice sociale : 99 €

  ⇒ Pas besoin de compter vos sous ? Permettez à d'autres de venir.


Vous trouverez en bas de document le lien de pré-inscription. Vous ne serez débité⋅e que si votre inscription est validée. Par sécurité, nous ne stockons que la référence d’autorisation de paiement, le reste est géré par l’organisme bancaire.

PS : Pour soutenir le travail de fond (coordination, documentation, conception d’outils…), à partir du 2ème évènement Chatonnade auquel vous participez, il y aura une adhésion semestrielle libre (avec montants suggérés).


## ⚕️ Mesures sanitaires ⚕️

- Si vous pouvez vous faire dépister l’avant veille de l’évènement (ou être vacciné), c’est top !
- Si vous présentez des symptômes du Covid avant l’évènement, prenez soin de votre santé et de celle des autres, ne venez-pas, nous vous rembourserons.
- Si vous présentez des symptômes du Covid dans les 7 jours suivant l’évènement, prévenez-nous, que nous transmettions l’information.
- Vos coordonnées vous seront demandées à l’inscription, elles pourront également servir à vous informer si vous devenez cas contact suite à l’évènement.
- Nous aérerons le lieu autant que possible durant toute la durée de l’évènement.
- De par les activités proposées durant l’évènement, il ne nous semble pas pertinent de réclamer le port du masque, ni d’en dissuader l’usage.


## 🏡 Lieu 🏡

Proche Bordeaux, accessible ligne Tram C. Adresse exacte communiquée après validation de votre inscription.

Plusieurs espaces seront accessibles pendant l’événement avec des priorités différentes (calme, échange…).


## 📅 Pré-inscription 📅

1. [Inscrivez-vous sur la billetterie](https://belong.li/home/event/T1InQHlAPhIJaLNZYHPe)
2. [Remplissez le questionnaire](https://framaforms.org/pre-inscription-aux-evenements-chatonnade-bordeaux-1623165406)
3. Patientez, l'orga en charge des inscriptions (**lead participantes** dans notre jargon) va choisir qui valider pour cet évènement. Nous sélectionnons les participant⋅e⋅s pour avoir une diversité de profils propice à l’épanouissement de tous⋅tes les présent⋅e⋅s.
4. Vous êtes validé⋅e ? C'est le moment de retourner voir les recommandations sanitaires pour l'évènement.

Vous n'êtes pas retenu⋅e pour cet évènement ? Le prochain sera peut-être le bon !

PS : si vous n'avez pas été sélectionné⋅e de façon répétée, n'hésitez pas à en parler aux orga.


## 🌻 D’autres questions ? 🌻

Email contact : chatonnade.bordeaux@protonmail.com
A très vite ! ♥

