title: Compta Wel'Cat
-------------------
# Comptabilité Wel'Cat

Au jour du : 21/06/2021

## Entrées :

### État actuel :
- Total (avec 11 participant⋅e⋅s) : 420 €
#### Détails :
- Don en amont : 1 x 100 € (espèce)
- Complément prix libre sur place : 20 € (espèce)
- Participation soutien : 2 x 55 € (Belong/stripe)
- Participation libre : 1 x 40 € (paypal)
- Participation équilibre : 3 x 30 € (Belong/stripe)
- Participation libre : 1 x 20 € (espèce)
- PaF Solidaire : 4 x 10 € (Belong/stripe)

## Sorties / Répartition :
### Légende :
- [ ] À rembourser/transmettre
- [x] Terminé

### État actuel :

- [x] 110,01€ de nourriture (7,33€/personne)
- [x] 46,20€ de Safer covid (10 auto-test & Gel Hydro)
- [x] 90€ de défraiement des déplacements des orgas qui ont amené du matériel
- [x] 50€ de participation aux frais du lieu d’accueil (nourriture, eau, électricité, loyer…)
- [x] 10€ de soutien au fonctionnement général de Chatonnade
- [x] 6€ de frais bancaires Stripe (2,5% des 240€ passés par Belong)

- [x] 60€ de trésorerie, fond de roulement pour les prochains événements
- [x] 40€ de gratification, répartis entre les 4 orgas pour leur temps
- [ ] 5€ de soutien à Framasoft (nous utilisons FramaForms et Mobilizon)
- [x] 3€ de provisions pour le panier Free Money (solidarité transversale entre participant⋅e⋅s)

Soutiens envisagés pour un prochain évènement :
- 3€ de soutiens au frais techniques des équipement mis à disposition (NAS)

<!--
Nous souhaitons aussi soutenir les personnes qui ont consacré de leur temps à rendre cet évènement possible.
Qu’elles puissent renouveler leur implication au-delà de ce que leur temps libre seul leur permettrait.
Et ainsi qu’elles puissent activement contribuer au succès de nos futurs évènements.

Rien qu’avec les temps de réunion et d’animation, nous dépassons les 50 heures pour cet évènement.
- [ ] 600 € à répartir entre les 4 orga pour le temps à concevoir, faire connaître, préparer, sécuriser et animer l’évènement.
-->

<!--

Au smic net horaire (8,11 de l’heure), cela donnerait déjà 405€.
Avec les cotisations et reversements, on dépasse les 600 €.
Facturé en entreprise, on dépasseraient les 2000€.
Bref, si nous ne voulons pas que l’implication des organisateurices se fasse au dépend
de leur finance, 600€ de soutien nous semble

Pour que leur implication de pèse pas sur leur vie mais l’allège.
Pour que cela soutienne leur vie plutôt que se faire à leur dépend.
Ni à proprement parlé bénévole, ni salarié, nous souhaitons soutenir les…
Pour que l’implication des organisateurices se fasse au service de leur vie
et non au détriment de cette dernière, nous visons de pouvoir les
soutenir financièrement dans un ordre de grandeur similaire au smic
pour les (> 50) heures consacrées à l’évènement.

-->
