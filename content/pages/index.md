title: Chatonnade Bordeaux
-------------------

![frontpage](img/bannieres/chatonnade-bdx.png)

**Chatonnade débarque à Bordeaux ! 😻**

### 💗 Chatonnade est une communauté qui organise des événements self-positifs. Nous y proposons un cadre structuré avec des espaces où :

- les différentes parties de soi sont les bienvenues, intégrées et honorées dans leur diversité,
- chacun·e est soutenu·e pour faire ce qui est juste pour soi à chaque instant,
- la culture du consentement et du prendre soin est au cœur de Chatonnade

### 🌈 Chatonnade s'inspire de mouvements tels que :

- Sexualité positive (S+)
- Burner (Nowhere & Burning Man)
- Rainbow Gathering
- LGBTQIA+
- Féminisme
- Cercles restauratifs
- CNV
- Slow Sex
- Tantra
- Chamanisme
- Kinky & BDSM
- Holacratie
- Écologie

### 🌠 Ce que je peux trouver en Chatonnade :

- liberté d’être
- liberté des corps
- liberté d’expression
- remises en questions
- émotions fortes
- écoute, soutien émotionnel
- confrontation à ses blessures
- cadre sécurisant propice à l’introspection
- diverses formes d’intimités
- acculturation au consentement
- échanges profonds et authentiques
- inclusivité
- remises en question de ses conditionnements, ses privilèges, des normes et injonctions sociétales…
- gouvernance partagée
- outils de résolution de conflit
- prix étudiés pour n’exclure personne
- une réflexion sur le rapport à l’argent

**Plus d’infos sur Chatonnade :** https://docs.chatonnade.org/

Tenté·e·s par l’aventure ?

A bientôt futur·e·s Chatounes !

## Qui sommes-nous ?

Actuellement l'équipe d'organisation c'est :

### Célia

C'est en 2017 que débute mon voyage dans le milieu de la Sexualité Positive, lorsque je décide de m'engager dans une relation ouverte avec mon partenaire. Depuis ce voyage est parsemé d'expériences diverses et complémentaires à la fois.

Je me suis formée et je pratique la Méditation Orgasmique. J'ai participé à deux éditions du festival Nowhere (Burning Man européen). C'est là-bas que je découvre des barrios proposant des ateliers autour de la sexualité, de l'intimité et du toucher. Et c'est aussi à cet endroit que je rencontre une personne qui développe depuis quelques mois des ateliers similaires sur Paris, et qui m'invite à y participer. C'est ainsi que j'intègre la communauté de Chatonnade en 2018.

### Céliane

Passionnée par la communication et les émotions depuis plus de 10 ans, j’ai choisi la psychologie comme ouverture au monde.

J’arpente les espaces féministes intersectionnels et d’éducation populaire depuis 3 ans, ça a été ma porte d’entrée vers les réflexions sur mon rapport aux normes sociales. J’ai découvert les relations non-exclusives, et cheminé encore plus sur la qualité de communication et les questions de consentement.
Dans mes explorations de dynamiques collectives, s’est cristallisée la part de moi qui est bergère des émotions et du bien-être du groupe, par la facilitation de cercles restauratifs, par les pratiques de Communication non Violente…

J’ai fait la connaissance du milieu Sexualité Positive en 2020 en Summer Kittens, et depuis j’œuvre à créer davantage d’espaces de bienveillance et d’épanouissement autour de moi.
Dans cette communauté, mon rêve serait de créer des groupes en non-mixité choisie pour l’empowerment et la créativité qu’ils ouvrent.

### Millicent

Socialisé homme blanc quasi-cis quasi-hétéro, avec un peu d’aide j’ai acquis une confiance bienvenue pour oser communiquer publiquement et oser recadrer les égos envahissants.
Je pars de très loin coté (in)aptitude sociale, ce qui m’aide à faire preuve d’empathie pour les maladresse inconscientes et à faire de la pédagogie dessus.

Informaticien passionné depuis l’enfance, j’aime soutenir ce qui à du sens à mes yeux grâce à mes compétences techniques.

Depuis 2012, je m’intéresse à la communication entre êtres humains (émotions, CNV, rapport au consentement…) , aux façons de relationner (polyamour, anarchie relationnelle), aux façons de vivre ensemble et de faire société (autogestion, intelligence collective, mode de gouvernance, rapports de pouvoir et de privilèges…). Entre 2018 et 2019, je découvre les cercles et postures restauratives, ainsi que l’écoute empathique. J’ai participé à des évènement et fréquenté des lieux à tendance autogérée, certains avec un cadre explicite pour faciliter l’autogestion (forum ouverts, Agile Open France, nouvel an SexPo au 30ème ciel) d’autre avec un cadre plus flou : geek camp, café poly, RADE (rencontre avec des enfants), rainbow gathering, squat et ZAD.

Hors informatique, j’ai l’impression d’être coutumier des acculturations informelles aux milieux qui m’intéressent, du coup je me sens proche de la culture Chatonnade sans avoir participé à un seul évènement officiellement estampillé Chatonnade, je me sens proche de la culture burner sans avoir participé à plus que des apéros burner et vécu avec des burners… Bref, entre incarnation et imposture, je navigue de mon mieux entre prendre la place de contribuer et laisser voir protéger la place de celleux qui peinent à en prendre.

<!--
### Khoukha

Travailleur social dans le quotidien, je suis engagée quotidiennement dans une démarche de prendre soin soucieuse de l'intégrité et du pouvoir d'agir de chacun.e.
Je suis investie avec passion dans l'art lyrique depuis plus de 10 ans et j'ai à coeur d'encourager l'expression de sa sensibilité par le corps et l'esprit.
J'ai découvert le milieu self-positif et Sexpo en 2019 et y ai trouvé un formidable terrain de déconstruction et d'expérimentation.
Aujourd'hui je souhaite contribuer à énergiser la création d'un espace de vie authentique où l'altérité, la bienveillance et la communication prennent tout leur sens.
-->

## Évènements

Un [code couleur 💚​💙​❤​🖤 ](https://docs.chatonnade.org/chatonnade/cadre/code-couleur)
et [thématique  🍑​🎶​🌗​👩💻 📚 🍻](https://docs.chatonnade.org/chatonnade/cadre/code-thematique)
est utilisé pour vous aider à repérer les évènements correspondant à ce que vous souhaitez vivre.

### Futurs
- 🎯 25 septembre 2021 : [Les cendres du phoenix](2021-09-25-les-cendres-du-phoenix.html)
- 💙 16 (+17?) octobre 2021 : [???]

### Passés
- 💚 19 juin 2021 : [Wel'Cat](2021-06-19-welcat.html)


## Ressources & contacts

- [**Groupe Mobilizon**](https://mobilizon.fr/@chatonnade_bordeaux)
- [**Page facebook**](https://www.facebook.com/chatonnade.bordeaux)
- E-mail : [**chatonnade.bordeaux@protonmail.com**](mailto:chatonnade.bordeaux@protonmail.com)

<!-- - [**Communauté sur Belong**](). -->
