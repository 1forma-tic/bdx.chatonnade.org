title: Mentions légales
-------------------
# Mentions légales

## Hébergement et direction technique
<address>
Millicent Billette - <a href="1forma-tic.fr">1forma-tic.fr</a>
<br/>26 Ter, rue André Lapelleterie, 33130, Bègles
<br/><a href="tel:0033770772770">+33&nbsp;(0)&nbsp;770&nbsp;772&nbsp;770</a>
<br/><a href="mailto:contact@1forma-tic.fr?subject=[legals]">contact@1forma-tic.fr</a>
<br/>SIRET : <a href="https://www.societe.com/societe/m-millicent-billette-de-villemeur-520193897.html"
                target="_blank">520&nbsp;193&nbsp;897&nbsp;00037</a>
</address>

## Politique de confidentialité

Site sans cookies, <a href="https://blogantipub.wordpress.com/charte-des-sites-sans-pub/" target="_blank">sans publicités</a>, sans collecte de données personnelles.

Seuls les logs de connexion (contenant votre adresse IP) sont conservés dans un but d'analyse du traffic. </br> Ils ne sont communiqués à personne et sont supprimés au bout de 15 jours.

## Crédits et licences

L'__architecture technique__ du site est placé dans le domaine public.

